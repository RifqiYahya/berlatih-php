<?php
function tentukan_nilai($number)
{
    //  kode disini
    if (85 <= $number){
      return 'Sangat Baik';
    } else if (70 <= $number) {
      return 'Baik';
    } else if (60 <= $number) {
      return 'Cukup';
    } else {
      return 'Kurang';
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
