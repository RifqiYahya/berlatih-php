<?php
function ubah_huruf($string){
//kode di sini
  $chars = str_split($string);
  for ($i = 0; $i < count($chars); $i++){
    $chars[$i]++;
  }

  return implode($chars);
}

//TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
